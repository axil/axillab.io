#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = "axil"
SITENAME = "Over the line"
SITE_TAGLINE = "Yeah, well, you know, that's just, like, my opinion, man."
SITEURL = ""
TIMEZONE = "Europe/Athens"
SHOW_AUTHOR = False
DEFAULT_DATE = "fs"
DEFAULT_DATE_FORMAT = "%Y"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
FEED_ALL_RSS = None
CATEGORY_FEED_ATOM = None
CATEGORY_FEED_RSS = None
TAG_FEED_RSS = None
TAG_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

LOCALE = ("en_US.utf8", "el_GR.utf8")
DEFAULT_LANG = "en"
SITE_SOURCE = "https://gitlab.com/axil/axil.gitlab.io"
SITE_ICON = "/images/favicon.ico"

MARKDOWN = {
    "extension_configs": {
        "markdown.extensions.codehilite": {"css_class": "codehilite"},
        "markdown.extensions.extra": {},
        "markdown.extensions.toc": {},
    },
    "output_format": "html5",
}

THEME = "../themes/pelican-uberspot"

OUTPUT_PATH = "public"
PATH = "content"

STATIC_PATHS = ["images", "files", "theme/fonts", ".well-known"]

ARTICLE_PATHS = ["posts"]
ARTICLE_URL =           "{lang}/blog/{date:%Y}/{slug}/"
ARTICLE_SAVE_AS =       "{lang}/blog/{date:%Y}/{slug}/index.html"
ARTICLE_LANG_URL =      "{lang}/blog/{date:%Y}/{slug}/"
ARTICLE_LANG_SAVE_AS =  "{lang}/blog/{date:%Y}/{slug}/index.html"

PAGE_PATHS = ["pages"]
PAGE_URL =          "{slug}/"
PAGE_SAVE_AS =      "{slug}/index.html"
PAGE_LANG_URL =     "{slug}/"
PAGE_LANG_SAVE_AS = "{slug}/index.html"

PLUGIN_PATHS = ["../pelican-plugins"]

PLUGINS = [
    "post_stats",
    "tipue_search",
    "neighbors",
]

DIRECT_TEMPLATES = (("index", "tags", "categories", "authors", "archives", "search"))

SOCIAL = [
        ["Mastodon", "https://linuxrocks.online/@axil"],
        ["Diaspora", "https://librenet.gr/people/089b758047580132f8c70093634e36ae"],
        ["GitLab", "https://gitlab.com/axil"],
        ["GitHub", "https://github.com/axilleas"],
        ["Twitter", "https://twitter.com/_axil"],
        ["RSS", "/rss"],
]

## Development settings
RELATIVE_URLS = True
LOAD_CONTENT_CACHE = False
WITH_FUTURE_DATES = True
