#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

SITEURL = 'https://axilleas.me'
DOMAIN = 'axilleas.me'

THEME = "themes/pelican-uberspot"
PLUGIN_PATHS = ["pelican-plugins"]

FEED_DOMAIN = SITEURL
FEED_ALL_RSS = 'feeds/all.rss.xml'
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_RSS = 'feeds/{slug}.rss.xml'
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'
TAG_FEED_RSS = 'feeds/{slug}.rss.xml'
TAG_FEED_ATOM = 'feeds/{slug}.atom.xml'

DISQUS_SITENAME = 'kissmyarch'
MATOMO_URL = 'piwik.axilleas.me'
MATOMO_SITE_ID = 1
#JS_HTTPS =  True
