# Blog axilleas.me

Built with [pelican](http://docs.getpelican.com), using the
[uberspot](https://gitlab.com/axil/pelican-uberspot) theme.

[Plugins](https://github.com/getpelican/pelican-plugins/) used:

- post_stats
- tipue_search
- neighbors

## Clone

```bash
git clone --recursive https://gitlab.com/axil/axil.gitlab.io.git
```

## Development

Create a virtualenv:

```sh
virtualenv .venv
```

Install pelican and plugin dependencies:

```bash
pip install -r requirements.txt
```

Hack with:

```
make devserver
```

Open <http://localhost:8000>.

## Production

Just push any changes and GitLab CI/CD will start building the project.

In general:

1. Install pelican and plugin dependencies:

   ```bash
   pip install -r requirements.txt
   ```

1. Generate the site:

   ```bash
   make publish
   ```

   The generated HTML files will be in the `public/` directory.

## License

Except where otherwise noted, content on this site is licensed under a
[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
