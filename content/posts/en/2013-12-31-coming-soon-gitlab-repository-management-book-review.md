---
title: Coming soon book review - GitLab repository management
tags: gitlab, book, review
category: reviews
---

![GitLab Repository Management](https://www.packtpub.com/sites/default/files/1794OS.jpg)

If you don't already know it, [Jonathan M. Hethey][twitter] has recently published a [book][] 
about GitLab on packtpub. I am in the process of reviewing it, so wait for a new post very soon :)

I'll attach the preface so you can get a glimpse of what the book covers.

> In this book, we will take a tour of the version control system GitLab, which is based
> on Git. It's open source and has many great features that we will learn chapter wise
> as listed here. First, we will take a look at what GitLab can do for us, and later we
> will see how we can install it on a server and configure it to match our needs.
> The next step is to take a close look at the web interface, and then we will learn
> how to handle permissions and teams, document our code, track issues, and show
> example workflows.
> Lastly, we will learn how to perform maintenance for our installation, including the
> creation of backups and upgrading to the most recent version. We will also cover
> getting in touch with the community and developers on the respective channels.

Btw, there is a limited offer on packtpub untill this Friday 3rd January, so you can grab an e-book copy
for only $5.

[book]: https://bit.ly/1fTUYMy
[twitter]: https://twitter.com/JonathanMH_com
