---
title: Accepted for GSoC 2013
slug: accepted-for-gsoc-2013
category: opensource
tags: gsoc, fedora, gitlab, superexcited
---

![gsoc-logo](https://1-ps.googleusercontent.com/sx/s.google-melange.appspot.com/www.google-melange.com/soc/content/2-1-20130521/images/gsoc/logo/924x156xbanner-gsoc2013.png.pagespeed.ic.Z9V_lgyiqp.jpg)

Sooo, I am happy to announce that I got accepted to this year's Google Summer of Code!
Couldn't be more happy right now :)

For those that have no idea, here is my [proposal][] on Fedora's wiki ([my user][]). 
I have in mind to make a follow post on how I dealt with all this the past three months.

Special thanks to [John Giannelos][troll] for pressuring me to apply :p

 
[proposal]: https://fedoraproject.org/wiki/GSOC_2013/Student_Application_Axilleas/Gitlab%28463%29
[my user]: https://fedoraproject.org/wiki/User:Axilleas 
[troll]: https://github.com/johngian
