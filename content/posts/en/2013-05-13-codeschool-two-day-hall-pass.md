---
title: Codeschool offers 2-day hall pass to all courses
slug: codeschool-two-day-hall-pass
category: news
tags: codeschool, ruby, courses, javascript, html, css
---

![codeschool-logo](https://d1tijy5l7mg5kk.cloudfront.net/assets/press_kit/logo-full-text-842f207391207c249d46b10fb166a8cb.png)

  >[Code School][] is an online learning platform that teaches a variety of
  >programming and web design skills. Courses range from beginner to advanced
  >levels and you get to earn rewards and badges as you learn.

Courses cover a variety of technologies including among others `Git`, `Ruby`, 
`Backbone.js`, `Sass`, `Rails`, `jQuery`. Each course has a unique environment 
and some their own theme song, which is kinda cool. Here is the [special page][jingles] where 
you can find all these songs. 

Normally, there is a monthly [subscription][enrollment] that costs only 25$ and 
grants you access to all courses and screencasts, with download links for the 
videos and the presentations.

The current offer covers a 2 day free pass for all courses and screencasts. [Here][mylink] 
is my affiliate link[^afflink] to get you started (shameless promo :p). 
For each person that signs up, you get two more free days until you reach a 
limit of 30. During this weekend I finished the [Ruby Bits][ruby-bits] and [Git Real][git-real]
courses and I have to admit I learned a lot. My next goal is the famous [Rails for Zombies][zombies] 
which I am looking forward to complete!


So, what are you waiting for? [Go][mylink] and learn some realy cool stuff!


[^afflink]: I got an email from codeschool about this offer and there doesn't seem to be a clear url to reach it. That's why I am posting the affiliate url.

[git-real]: https://www.codeschool.com/courses/git-real
[enrollment]: https://www.codeschool.com/enroll
[jingles]: https://www.codeschool.com/jingles
[zombies]: https://www.codeschool.com/courses/rails-for-zombies-redux
[ruby-bits]: https://www.codeschool.com/courses/ruby-bits
[report-card]: https://www.codeschool.com/users/axil
[mylink]: https://go.codeschool.com/3UBpUQ
[Code School]: https://www.codeschool.com/
[codeschool-logo]: https://d1tijy5l7mg5kk.cloudfront.net/assets/press_kit/logo-full-text-842f207391207c249d46b10fb166a8cb.png
