---
title: Nominated for Space Apps Challenge 2013
slug: nasa-spaceappschallenge-2013
category: news
tags: nasa, hsgr, django, spinach, popeye
lang: en
---

***Update***: The Popeye on Mars project came [first][] in "Best Mission Concept" 
category. Congrats guys!

During the weekend of 20th and 21st of April we gathered at [hackerpace.gr](https://hackerspace.gr)
and started hacking for NASA's space apps challenge. From their [webpage](https://spaceappschallenge.org/about/):

  >The International Space Apps Challenge is an international mass collaboration 
  >focused on space exploration that takes place over 48-hours in cities around 
  >the world. The event embraces collaborative problem solving with a goal of 
  >producing relevant open-source solutions to address global needs applicable to
  >both life on Earth and life in space. NASA is leading this global collaboration 
  >along with a number of additional government collaborators and 100+ local 
  >partner organizations.

16 people, 2 teams, 2 days, 2 globally nominated projects:
`popeye on mars` and `anapNEO`. See [here][photos] some photos. 
Thank you [mozilla](https://mozilla.org) for all the pizzas and souvlakia :)


# Popeye on Mars
Our solution is a deployable, reusable spinach greenhouse for Mars.
Internally, a fully equipped aeroponic system operates for ~45 days,
having all the needed resources, sensors and electronic systems to
stabilize the internal environment and help the spinach growth. Also,
there are systems for harvesting produced oxygen during the process
and the plants at the end of it. Externally, photovoltaic panels
provide power, while several cover layers protect the system against
Mars extreme conditions.

 - [Project page](https://spaceappschallenge.org/project/pom)
 - [Video](https://www.youtube.com/watch?feature=player_embedded&v=kBl5lzwcYlQ)


# anapNEO

anapNEO is a web app build on Django and HTML, CSS, JS that enables
the submission and ranking of NEO findings. Focused on UX simplicity
and streamlining of the registration and submission process, a user
signs up easily using Persona, and creates a user account (with basic
information). She then lands on a (public too!) Dashboard that
displays all latest submissions and can submit a new finding. You can
also vote on findings affecting the score and (yet to implement)
expose those on an API.

 - [Project page](https://spaceappschallenge.org/project/anapneo)
 - [Video](https://www.youtube.com/watch?feature=player_embedded&v=u5eSPSy4ix8)


[photos]: https://plus.google.com/u/0/photos/117312389370621956919/albums/5868971703249663889 "Space App Challenge 2013 at hackerspace.gr"
[first]: https://www.nasa.gov/home/hqnews/2013/may/HQ_13-152_Space_App_Challenge_winners.html
