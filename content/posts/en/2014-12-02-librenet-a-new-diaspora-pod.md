---
title: librenet.gr - a new Diaspora pod
date: 2014-12-04 20:00
slug: librenet-a-new-diaspora-pod
tags: ruby, rails, diaspora, centos, systemd, devops, ansible
category: opensource
lang: en
Status: draft
---

[TOC]

Hello world! A new Diaspora* pod has come to life. It was initially deployed
for the Greek users, but the registrations are open so anyone can sign up for
a new account and connect to the Diaspora network.

Visit <https://librenet.gr> now!

At this time of writing we host around 300 users. And that has happened in less
in a month!

# librenet.gr

## The team



## Privacy and security are important to us

## What we had to overcome

## Custom changes

<https://librenet.gr/posts/2480>

## How we deploy to production

For all the repetitive administrative tasks we use Ansible.

## Using Vagrant to set up a staging environment

# Diaspora

## Stable vs Develop

## How to contribute

The whole idea is based on open source standards, so any little help has a
great impact since this is a community project.

If you are looking for ways to contribute, check [this awesome post][contrib]
by Fla, a core contributor of Diaspora.


[contrib]: https://diaspora-fr.org/posts/793785 "There are tons of way to #contribute to the diaspora* project"
