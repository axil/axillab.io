---
title: GitLab - accepted patches
status: hidden
---

This is a reminder page for the patches I sent and were accepted to GitLab.


| Description             |   commit    | Pull Request |
|:------------------------|:------------|:------------:|
| Documentation fix       | [655e68a][] |   [2998][]   |
| Remove function         | [72d4837][] |   [3057][]   |
| Check Redis version     | [7305d98][] |   [3602][]   |
| os_name systemd support | [bd40579][] |   [3603][]   |
| Typo in gitlab.yml      | [4fe1c89][] |   [3604][]   |
| Typo in deploy_keys/haml| [f60486a][] |   [3765][]   |
| Update installation.md  | [3a5d90c][] |   [3766][]   |
| Update docs             | [830afbc][] |   [3760][]   |


[See the full list on github.][commits]

[3760]: https://github.com/gitlabhq/gitlabhq/pull/3760
[830afbc]: https://github.com/gitlabhq/gitlabhq/commit/830afbc3d2cff5c8c22f18c4b266d08019b34039

[3a5d90c]: https://github.com/gitlabhq/gitlabhq/commit/3a5d90c4a3c1b1cf6fae6e1ef9210593961971d1
[3766]: https://github.com/gitlabhq/gitlabhq/pull/3766

[f60486a]: https://github.com/gitlabhq/gitlabhq/commit/f60486a0c42a7167d283bc64a8fbcdb0fa5533a2
[3765]: https://github.com/gitlabhq/gitlabhq/pull/3765

[7305d98]: https://github.com/gitlabhq/gitlabhq/commit/7305d98c8e486b47ba6e90a9a4fe3b223d519827
[3602]: https://github.com/gitlabhq/gitlabhq/pull/3602

[4fe1c89]: https://github.com/gitlabhq/gitlabhq/commit/4fe1c8941e3b07488501c84589dd364d00cf8f20
[3604]: https://github.com/gitlabhq/gitlabhq/pull/3604

[bd40579]: https://github.com/gitlabhq/gitlabhq/commit/bd4057910b117b8b44fd45b771ffd67316421ea9
[3603]: https://github.com/gitlabhq/gitlabhq/pull/3603

[72d4837]: https://github.com/gitlabhq/gitlabhq/commit/72d48376ab81ee216ccec0cd65b114e85e63115e
[3057]: https://github.com/gitlabhq/gitlabhq/pull/3057

[655e68a]: https://github.com/gitlabhq/gitlabhq/commit/655e68a0adc391013bea1dd4ef3838edde7c3f97
[2998]: https://github.com/gitlabhq/gitlabhq/pull/2998

[commits]: https://github.com/gitlabhq/gitlabhq/commits/?author=axilleas
