---
title: whoami
slug: about
---

Hey there! My name is Achilleas Pipinellis and you reached the about me page :)

Here you will find some info about the projects I contribute to, as well as
some personal things, like what my hobbies are.

You can contact me via email or reach to me through the social media. See at
the bottom of this page for contact details.

Find some more info about me in my [CV]({static}/files/CV_Achilleas_Pipinellis.pdf).

## Contributions to open source

### GitLab

[GitLab][gl-site] is an open source Git repository application, built on
Ruby on Rails. I have been using it since version 2.0 and started
contributing in version 4.0.
On [April 2014][twitter-ann] I became a member of the community [core-team][]
as GitLab recipes lead. As of December 2015 I've been hired to work full-time
as a Technical Writer, that's why you'll see me helping mainly with things
involving documentation. If you want to ping me on GitLab.com, use `@axil` and
I'll respond as soon as possible.

### Fedora

In early 2013 I started getting involved with [Fedora][], mainly because
of the [GSoC 2013](|static|/posts/en/2013-04-07-bringing-gitlab-to-fedora.md) program.
It has a vast community with many developers, sysadmins and users and a lot
of areas you can contribute to.
At that time, I maintained some [packages][fedora-pkgdb], mostly rubygems.

I had the pleasure to participate in the Google Summer of Code program
in 2013 and 2014 with the Fedora project.

See more info in my [Fedora wiki page][].

### Arch Linux

I've been using [Arch Linux][] as my main distribution since 2009.
I maintain several packages at the [AUR][] and I try to contribute to the
[Archwiki][] whenever I can. Take also a look at the [archlinuxgr repository][archlinuxgr]
which has popular packages that aren't in the official repos. I have also
contributed to the localization of pacman and the AUR interface. Check my
transifex profile at Contact Details.

## Technical writing

I have always liked writing about technical stuff and my involvement with system
administration has helped a lot.

You can find tutorials I write for DigitalOcean by [visiting my profile][dopro].

### Book author

Yes, as of September 2015 I'm also a book author :)

For the time being my only published book is from Packtpub and is about GitHub
(that was before I joined GitLab 😛):

[GitHub Essentials - Unleash the power of collaborative workﬂow development using GitHub, one step at a time][ghebook]

You can read a sample chapter on [slideshare][ghebook-sl] and read another
chapter in an [article post][ghebook-post] by Packtpub.

## Workstation

I am a proud owner of a Thinkpad x260 running Arch Linux.

```
                  ##                    OS: Arch Linux x86_64
                 ####                   Hostname: thedude
                ######                  Uptime: 2 days, 23:43
               ########                 Kernel: 4.18.5-arch1-1-ARCH
              ##########                Shell: /usr/bin/zsh
             ############               Packages: 2298
            ##############              DE: KDE
           ################             GTK2 theme: Breeze
          ##################            GTK2 icon theme: Numix
         ####################           GTK2 font name: ABeeZee Regular 10
        ######################          RAM: 7611M / 15911M (47%)
       #########      #########         CPU: Intel(R) Core(TM) i7-6600U CPU @ 2.60GHz
      ##########      ##########        Boot: 92M / 511M (18%) (vfat)
     ###########      ###########       Root: 426G / 469G (96%) (ext4)
    ##########          ##########
   #######                  #######
  ####                          ####
 ###                              ###
```

> Output using the [alsi][] package.

## Hobbies/Interests

### Aikido

My greatest love is [Aikido][], a japanese martial art. I first started in
January 2010 and have attended a bunch of seminars since. There is a
site of our [dojo][tokaidogr] and some social pages ([facebook][fb-tok],
[twitter][twit-tok], [youtube][yt-tok]). You can check some photos in the
facebook page. My limited skills on graphical design led me to do the artwork of
our dojo as well. Source files can be found on [github][tokaido-artwork].

### Photography

I have an old Olympus DSLR and whenever I have the chance I get pictures.
My main interest is my pets, but I also take landscapes as well. Some day
I hope to buy a more advanced camera which supports custom firmware in
order to hack it :p

## Favorites

### Movies

I like watching movies and TV series. I always enjoy the Asian cinema and
the Japanese Anime movies and series. Did I mention that I also like manga?

With no particular order (well, except from the first one), some of my
favorite movies are:

  - The Big Lebowski
  - Sin City
  - 12 Angry Men
  - Drive
  - Nine Queens
  - Paprika
  - Weekend at Bernie's
  - Black Dynamite
  - 13 Assassins
  - Hot Fuzz
  - Spirited Away

### Music

I used to play keyboard[^keyboard] in a band, but unfortunately I haven't
practiced for a looong time. I also own an electric guitar.

Other than playing, I like listening to music. When working or studying,
most of the times I listen to jazz because I find it non distracting.
Other music genres that I like are funk, blues, classical, and progressive rock/metal.
Often you'll also find me listening to 8bit, dubstep, and electronic :)

If I had to pick one band that I miss not seeing live, that would be Rush.

## Contact details

- Email       : `hello@axilleas.me`
- PGP key     : [B77EE63D467086985575020A0B44204ED1D04AA9.asc]({static}/files/B77EE63D467086985575020A0B44204ED1D04AA9.asc)
- Mastodon    : [axil@linuxrocks.online](https://linuxrocks.online/@axil)
- Diaspora    : [thedude@librenet.gr](https://librenet.gr/people/089b758047580132f8c70093634e36ae)
- GitLab      : <https://gitlab.com/u/axil>
- GitHub      : <https://github.com/axilleas>
- Twitter     : <https://twitter.com/_axil>
- IRC         : `axil42` (freenode, mozilla)

[^keyboard]: Proud owner of a Roland Juno-D.

[Arch Linux]: https://archlinux.org
[AUR]: https://aur.archlinux.org/packages/?SeB=m&K=axil42 "Packages I maintain at Arch User Repository"
[Archwiki]: https://wiki.archlinux.org/index.php/Special:Contributions/Maevius "My contributions to the Archwiki"
[archlinuxgr]: https://archlinuxgr.tiven.org/archlinux/
[Fedora]: https://fedoraproject.org
[fedora-pkgdb]: https://admin.fedoraproject.org/pkgdb/packager/axilleas/ "My packages at Fedora"
[Aikido]: https://en.wikipedia.org/wiki/Aikido "Aikido at Wikipedia"
[tokaidogr]: https://tokaido.gr "Aikido Tokaido Dojo"
[fb-tok]: https://facebook.com/tokaidogr
[twit-tok]: https://twitter.com/tokaidogr
[yt-tok]: https://youtube.com/tokaidogr
[tokaido-photos]: https://www.facebook.com/tokaidogr/photos_stream
[tokaido-artwork]: https://github.com/tokaidogr/artwork
[gl-site]: https://about.gitlab.com/ "GitLab home page"
[core-team]: https://about.gitlab.com/core-team/ "GitLab core-team"
[Fedora wiki page]: https://fedoraproject.org/wiki/User:Axilleas
[twitter-ann]: https://twitter.com/gitlab/status/453461777821736960 "GitLab twitter announcement of me joining the core team"
[dopro]: https://www.digitalocean.com/community/users/thedude?primary_filter=tutorials "DigitalOcean tutorials"
[ghebook]: https://www.packtpub.com/application-development/github-essentials "My book 'GitHub Essentials' by Packtpub"
[ghebook-sl]: https://www.slideshare.net/Products123/github-essentials-sample-chapter "Free sample chapter for 'GitHub Essentials' book"
[ghebook-post]: https://www.packtpub.com/books/content/collaboration-using-github-workflow "Article on Collaboration using the GitHub workflow"
[alsi]: https://aur.archlinux.org/packages/alsi/ "A configurable system information tool for Arch Linux"
