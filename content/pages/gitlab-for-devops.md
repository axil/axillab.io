---
title: GitLab for DevOps
slug: gitlab-for-devops
status: hidden
---

This is a book written to tackle everyday situations in a Developer and Operations
engineer life. From installation to complex configuration, this is a book for you.

## Why another GitLab book

I have been following the GitLab community since its very beginning in early 2012.
GitLab provides comprehensive documentation, but some things cannot be put in
the docs.

During my involvement in helping the community answering questions, I found out
that there is a piece missing from GitLab to completely bond with its users.
Thus, I decided to write down all the missing concepts that would ultimately
form guides rather than plain docs.

Of course like always, if I find that a piece also belongs to the documentation
I will submit an MR to GitLab.

## Audience

The book targets experienced system administrators. Simple concepts are left out
since they are already documented by GitLab upstream. You can read the contents
of the book in its [Leanpub page][buy].

If you feel you need to first grasp some basic to advanced ideas regarding GitLab,
I urge you to buy [GitLab Cookbook by Jeroen van Baarsen][gitlab-cookbook], a
fellow member of the GitLab core team.

## Publisher, license and price

I decided to go with Leanpub as it gives its authors the ability to publish early
and publish often. That means that the book will be a Work In Progress for some
time until it gets fully finished.

Since GitLab is under super active development, publishing on Leanpub gives me
the opportunity to edit whenever I please to do so, which means no out-dated
chapters. I can also use my favorite tools: vim, git and markdown which means
greater productivity.

Buy once and get free updates for life!

I'm strongly thinking of publishing a free html version of the book under an
open source license when it gets finished, but till then there is a minimum
price for the e-book. This is to get me going with testing GitLab configuration
and installation to services that cost money and be able to dedicate a significant
amount of my time in writing this.

[Buy GitLab for DevOps on Leanpub][buy]

[buy]: https://leanpub.com/gitlab-for-devops "GitLab for DevOps on Leanpub"
[gitlab-cookbook]: https://www.packtpub.com/application-development/gitlab-cookbook "GitLab Cookbook by Jeroen van Baarsen on Packtpub"
